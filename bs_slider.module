<?php

/**
 * @file
 * Main module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function bs_slider_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the bs_slider module.
    case 'help.page.bs_slider':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('BS Slider is plug-able slider, carousel or gallery framework.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_theme().
 */
function bs_slider_theme() {
  return [
    'bs_slider' => [
      'render element' => 'element',
    ],
  ];;
}

/**
 * Implements hook_theme_suggestions_HOOK() for bs_slider.
 */
function bs_slider_theme_suggestions_bs_slider(array $variables) {
  $suggestions = [];

  // BS Slider plugin name.
  /** @var \Drupal\bs_slider\BsSliderConfigurationManagerInterface $bs_slider_configuration_manager */
  $bs_slider_configuration_manager = \Drupal::service('bs_slider_configuration.manager');
  /** @var \Drupal\bs_slider\Plugin\BsSliderInterface $bs_slider */
  if ($bs_slider = $bs_slider_configuration_manager->entityLoad($variables['element']['#settings']['bs_slider'])) {
    $suggestions[] = 'bs_slider__' . $bs_slider->getPluginId();

    // BS Slider plugin instance machine name.
    if ($bs_slider->getPluginId() !== $variables['element']['#settings']['bs_slider']) {
      $suggestions[] = 'bs_slider__' . $variables['element']['#settings']['bs_slider'];
    }

    // Finally we combine plugin id and configuration machine name.
    $suggestions[] = 'bs_slider__' . $bs_slider->getPluginId() . '__' . $variables['element']['#settings']['bs_slider'];
  }

  return $suggestions;
}

/**
 * Prepares variables for bs_slider template.
 *
 * Default template: bs-slider.html.twig.
 *
 * @param array $variables
 */
function template_preprocess_bs_slider(&$variables) {
  if (!empty($variables['element']['#settings']['bs_slider'])) {
    if (isset($variables['element']['#field_name'])) {
      $variables['field_name'] = $variables['element']['#field_name'];
    }

    /** @var \Drupal\bs_slider\BsSliderConfigurationManagerInterface $bs_slider_configuration_manager */
    $bs_slider_configuration_manager = \Drupal::service('bs_slider_configuration.manager');
    /** @var \Drupal\bs_slider\Entity\BsSliderConfigurationInterface $bs_slider */
    if ($bs_slider = $bs_slider_configuration_manager->entityLoad($variables['element']['#settings']['bs_slider'])) {
      $variables['plugin_id'] = $bs_slider->getPluginID();
      $variables['bs_slider_id'] = $bs_slider->id();
      $variables['bs_slider'] = $bs_slider;

      /** @var \Drupal\bs_slider\Plugin\BsSliderManager $plugin_manager */
      $plugin_manager = \Drupal::service('plugin.manager.bs_slider');
      /** @var \Drupal\bs_slider\Plugin\BsSliderInterface $plugin */
      $plugin = $plugin_manager->createInstance($bs_slider->getPluginId(), $bs_slider->getOptions());
      $plugin->preprocess($variables);
    }
  }
}

