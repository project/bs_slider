<?php

namespace Drupal\bs_slider;

/**
 * Defines re-usable services and functions for slick plugins.
 */
interface BsSliderConfigurationManagerInterface {

  /**
   * Returns the renderer.
   *
   * @return \Drupal\Core\Render\RendererInterface
   */
  public function getRenderer();

  /**
   * Returns any config, or keyed by the $setting_name.
   *
   * @param string $setting_name
   * @param string $settings
   */
  public function configLoad(string $setting_name = '', string $settings = 'bs_slider.options');

  /**
   * Returns a shortcut for loading a config entity: image_style, bs_slider, etc.
   *
   * @param $id
   */
  public function entityLoad($id);

  /**
   * Returns a shortcut for loading multiple configuration entities.
   *
   * @param $ids
   */
  public function entityLoadMultiple($ids = NULL);

  /**
   * Returns multiple configuration entities filtered by properties.
   *
   * @param array $values
   *   Array of properties.
   */
  public function entityLoadByProperties(array $values = []);

  /**
   * Returns an array of all available slider options.
   *
   * Useful for select and similar options value.
   *
   * @return array
   *   All available option sets, e.g. ['machine_name' => 'Label'].
   */
  public function getAllOptionSet();

  /**
   * Returns an array of all available slider options filtered by slider
   * properties.
   *
   * Useful for select and similar options value.
   *
   * @param array $values
   *   Array of properties.
   *
   * @return array
   *   All available option sets, e.g. ['machine_name' => 'Label'].
   */
  public function getAllOptionSetByProperties(array $values = []);

  /**
   * Returns the options that matches given bs_slider configuration name.
   *
   * @param string $name
   *   The bs_slider configuration name to be found.
   *
   * @return array
   *   Array of options from bs_slider configuration.
   */
  public function getOptionSet(string $name);

  /**
   * Returns description about bs_slider configuration optionset options for
   * bs_slider select field.
   *
   * @param array|null $bs_slider_optionset_options
   *   Available bs_slider configurations description. If NULL then all options
   *   set will be loaded first.
   *
   * @return string
   *   Description for bs_slider select form field.
   */
  public function getOptionSetDescription(array $bs_slider_optionset_options = NULL);

  /**
   * Get bs_slider plugin.
   *
   * @param string $id
   *  BS Slider configuration id.
   *
   * @return \Drupal\bs_slider\Plugin\BsSliderInterface
   *   BS Slider plugin object or NULL if configuration or plugin do not exist.
   */
  public function getPlugin(string $id);

}
