<?php

namespace Drupal\bs_slider\Plugin;

use Drupal\bs_slider\BsSliderPluginOptionInterface;
use Drupal\bs_slider\Entity\BsSliderConfigurationInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for BS Slider plugins.
 */
abstract class BsSliderBase extends PluginBase implements BsSliderInterface, ContainerFactoryPluginInterface {

  /**
   * Constructs a new BS Slider Bootstrap Carousel object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) { }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) { }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildPluginOptionsForm(array $form, FormStateInterface $form_state, BsSliderPluginOptionInterface $plugin_option) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->pluginDefinition['description'];
  }

  public function preprocess(array &$variables) {}

  /**
   * Build slider render array.
   *
   * @param array $items
   *   Slider items.
   * @param BsSliderConfigurationInterface $bs_slider
   *   BS Slider configuration entity.
   *
   * @return array
   *   Render array.
   */
  protected function buildSliderArray(array $items, BsSliderConfigurationInterface $bs_slider) {
    return [
      '#theme' => 'bs_slider',
      '#id' => Html::getUniqueId('bs-slider'),
      '#items' => $items,
      '#settings' => [
        'bs_slider' => $bs_slider->id(),
      ],
      '#cache' => [
        'tags' =>  array_merge([], $bs_slider->getCacheTags()),
      ],
    ];
  }

  /**
   * Default implementation of a view that is suitable for carousel sliders -
   * sliders with just one dimension.
   */
  public function view(array &$build, BsSliderConfigurationInterface $bs_slider, array $options = []) {
    $items = [];
    foreach (Element::children($build) as $delta) {
      $item = $build[$delta];

      // Case when rendering is coming from views display. We will take content
      // and ditch attributes because we do not need it.
      if (isset($item['content'])) {
        $item = $item['content'];
      }

      if (!empty($build[$delta]['#view_mode'])) {
        $item['#view_mode'] = $options['view_mode'];

        if (isset($item['#cache']['keys'][3])) {
          $item['#cache']['keys'][3] = $options['view_mode'];
        }
      }

      $items[$delta] = $item;
    }

    $build = $this->buildSliderArray($items, $bs_slider);
  }

}
