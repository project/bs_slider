<?php

namespace Drupal\bs_slider\Plugin\Field\FieldFormatter;

use Drupal\bs_slider\BsSliderConfigurationManagerInterface;
use Drupal\bs_slider\BsSliderPluginOptionInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for bs_slider entity reference formatters with field details.
 */
abstract class BsSliderEntityReferenceFormatterBase extends EntityReferenceEntityFormatter implements BsSliderPluginOptionInterface, ContainerFactoryPluginInterface {

  use BsSliderFormatterTrait;

  /**
   * Constructs a BsSliderEntityReferenceFormatterBase instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\bs_slider\BsSliderConfigurationManagerInterface $manager
   *   BS slider manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, LoggerChannelFactoryInterface $logger_factory, EntityTypeManagerInterface $entity_type_manager, EntityDisplayRepositoryInterface $entity_display_repository, BsSliderConfigurationManagerInterface $manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $logger_factory, $entity_type_manager, $entity_display_repository);
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
      $container->get('bs_slider_configuration.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'bs_slider' => NULL,
      // Plugin can store it formatter settings in options sub array.
      // There is no way for plugin to expose default values to formatter.
      'options' => [],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginOptionValue($name) {
    $options = $this->getSetting('options');
    return $options[$name] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginOptions($name) {
    if ($name == 'target_field_view_modes') {
      return $this->entityDisplayRepository->getViewModeOptions($this->getFieldSetting('target_type'));
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return $this->getSettingsFormElements($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    if ($this->getSetting('bs_slider') && $bs_slider = $this->manager->entityLoad($this->getSetting('bs_slider'))) {
      $summary[] = $this->t('BS Slider: @name', ['@name' => $bs_slider->label()]);
    }

    $view_mode = $this->getPluginOptionValue('view_mode');
    $view_modes = $this->getPluginOptions('target_field_view_modes');
    $summary[] = t('Rendered as @mode', ['@mode' => $view_modes[$view_mode] ?? $view_mode]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Early opt-out if the field is empty.
    if ($items->isEmpty()) {
      return [];
    }

    $build = parent::viewElements($items, $langcode);

    $bs_slider = $this->manager->entityLoad($this->getSetting('bs_slider'));

    $plugin = $this->manager->getPlugin($this->getSetting('bs_slider'));
    $plugin->view($build, $bs_slider, $this->getSetting('options'));

    return $build;
  }

}
