<?php

namespace Drupal\bs_slider\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a BS Slider item annotation object.
 *
 * @see \Drupal\bs_slider\Plugin\BsSliderManager
 * @see plugin_api
 *
 * @Annotation
 */
class BsSlider extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
