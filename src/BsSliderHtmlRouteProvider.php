<?php

namespace Drupal\bs_slider;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for BS Slider configuration entities.
 *
 * @see \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see \Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class BsSliderHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    $duplicate_route = new Route($entity_type->getLinkTemplate('duplicate-form'),
      [
        '_entity_form' => 'bs_slider.duplicate',
        '_title' => 'Duplicate BS Slider configuration'
      ],
      [ '_permission' => 'administer bs_slider' ]
    );
    $collection->add('entity.bs_slider.duplicate_form', $duplicate_route);

    return $collection;
  }

}
