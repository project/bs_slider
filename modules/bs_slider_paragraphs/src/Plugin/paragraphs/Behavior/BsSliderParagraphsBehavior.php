<?php

namespace Drupal\bs_slider_paragraphs\Plugin\paragraphs\Behavior;

use Drupal\bs_slider\BsSliderConfigurationManager;
use Drupal\bs_slider\BsSliderPluginOptionInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides BS Slider paragraphs behavior plugin.
 *
 * @ParagraphsBehavior(
 *   id = "bs_slider",
 *   label = @Translation("BS Slider"),
 *   description = @Translation("BS Slider for paragraphs."),
 *   weight = 0
 * )
 */
class BsSliderParagraphsBehavior extends ParagraphsBehaviorBase implements BsSliderPluginOptionInterface, ContainerFactoryPluginInterface {

  /**
   * The BS Slider manager.
   *
   * @var \Drupal\bs_slider\BsSliderConfigurationManager
   */
  protected $bsSliderManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /** @var \Drupal\paragraphs\Entity\ParagraphsType */
  private $paragraph_type;

  /**
   * BsSliderParagraphsBehavior constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\bs_slider\BsSliderConfigurationManager $bs_slider_manager
   *   The BS Slider manager service.
   * @param \Drupal\core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   Current user for permissions scope.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityFieldManagerInterface $entity_field_manager, BsSliderConfigurationManager $bs_slider_manager, EntityTypeManagerInterface $entity_type_manager, EntityDisplayRepositoryInterface $entity_display_repository, ModuleHandlerInterface $module_handler, AccountProxyInterface $currentUser) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_field_manager);
    $this->bsSliderManager = $bs_slider_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->moduleHandler = $module_handler;
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_field.manager'),
      $container->get('bs_slider_configuration.manager'),
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
      $container->get('module_handler'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'field_name' => '',
      'bs_slider' => [],
      // Active plugin can store it formatter settings in options sub array.
      // There is no way for plugin to expose default values to formatter.
      'options' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginOptionValue($name) {
    if (isset($this->configuration['options'])) {
      return $this->configuration['options'][$name] ?? NULL;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginOptions($name) {
    if ($name == 'target_field_view_modes' && $this->paragraph_type) {
      return $this->getEntityRefFieldViewModes($this->paragraph_type, $this->configuration['field_name']);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $this->paragraph_type = $form_state->getFormObject()->getEntity();

    if ($this->paragraph_type->isNew()) {
      return [];
    }

    $bs_slider_configurations = $this->bsSliderManager->getAllOptionSet();

    // Show field mapping select form only if this entity has at least
    // one mappable field.
    $field_name_options = $this->getFieldsByCardinalityGreaterOne($this->paragraph_type);
    if ($field_name_options) {
      $form['field_name'] = [
        '#type' => 'select',
        '#title' => $this->t('Slider field'),
        '#description' => $this->t('Choose the field to be used for slider items.'),
        '#options' => $field_name_options,
        '#default_value' => $this->configuration['field_name'],
      ];

      // Select all chosen configurations and plugins from them.
      $active_bs_sliders = $form_state->getCompleteFormState()->getValue(['behavior_plugins', 'bs_slider', 'settings', 'bs_slider'], []);
      if (empty($active_bs_sliders)) {
        $active_bs_sliders = $this->configuration['bs_slider'];
      }
      else {
        foreach($active_bs_sliders as $key => $value) {
          if ($value['plugin'] === 0) {
            unset($active_bs_sliders[$key]);
          }
          else {
            // Take current form value if it exist, stored configuration if it
            // exist or empty array on the end.
            $active_bs_sliders[$key] = isset($value['options']) ? $value['options'] : (isset($this->configuration['bs_slider'][$key]) ? $this->configuration['bs_slider'][$key] : []);
          }
        }
      }

      // Attach plugin formatter form elements.
      /** @var \Drupal\bs_slider\Plugin\BsSliderManager $plugin_manager */
      $plugin_manager = \Drupal::service('plugin.manager.bs_slider');

      $form['bs_slider'] = [
        '#type' => 'fieldset',
        '#tree' => TRUE,
        '#title' => $this->t('Slider Configurations'),
        '#description' => $this->bsSliderManager->getOptionSetDescription(),
      ];
      foreach ($bs_slider_configurations as $bs_slider_configuration => $label) {
        $options_id = 'bs-slider-configuration--' . Html::cleanCssIdentifier($bs_slider_configuration);
        $form['bs_slider'][$bs_slider_configuration] = [
          '#prefix' => '<div id="' . $options_id . '">',
          '#suffix' => '</div>',
          '#tree' => TRUE,
          'plugin' => [
            '#type' => 'checkbox',
            '#title' => $label,
            '#default_value' => isset($active_bs_sliders[$bs_slider_configuration]),
            '#ajax' => [
              'callback' => [$this, 'updatePluginOptionsAjax'],
              'wrapper' => $options_id,
            ],
          ],
        ];

        // Attach plugin formatter form elements for enabled sliders.
        if (isset($active_bs_sliders[$bs_slider_configuration])) {
          // Add view_modes to configuration options which is needed for plugin
          // form building.
          $this->configuration['options'] = $active_bs_sliders[$bs_slider_configuration];

          // Use currently selected target field form value for field_name. This
          // will avoid problem when plugin is first time enabled because in
          // this case field_name value is undefined.
          $this->configuration['field_name'] = $form_state->getCompleteFormState()->getValue(['behavior_plugins', 'bs_slider', 'settings', 'field_name'], $this->configuration['field_name']);

          /** @var \Drupal\bs_slider\Entity\BsSliderConfigurationInterface $bs_slider */
          $bs_slider = $this->bsSliderManager->entityLoad($bs_slider_configuration);
          $options = $bs_slider->getOptions();
          /** @var \Drupal\bs_slider\Plugin\BsSliderInterface $plugin */
          $plugin = $plugin_manager->createInstance($bs_slider->getPluginId(), $options);
          $form['bs_slider'][$bs_slider_configuration]['options'] = $plugin->buildPluginOptionsForm($form, $form_state, $this);

          // Use seven panel CSS class to visually group plugin options.
          $form['bs_slider'][$bs_slider_configuration]['options']['#prefix'] = '<div class="panel">';
          $form['bs_slider'][$bs_slider_configuration]['options']['#suffix'] = '</div>';
        }
      }
    }
    else {
      $form['message'] = [
        '#type' => 'container',
        '#markup' => $this->t('There are no fields available with the cardinality greater than one. Please add at least one in the <a href=":link">Manage fields</a> page.', [
          ':link' => Url::fromRoute("entity.paragraph.field_ui_fields", [$this->paragraph_type->getEntityTypeId() => $this->paragraph_type->id()])
            ->toString(),
        ]),
        '#attributes' => [
          'class' => ['messages messages--error'],
        ],
      ];
    }

    return $form;
  }

  /**
   * Ajax callback that updates bs_slider plugin form part.
   */
  public function updatePluginOptionsAjax(array $form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $bs_slider_configuration = $triggering_element['#parents'][count($triggering_element['#parents']) - 2];
    return $form['behavior_plugins']['bs_slider']['settings']['bs_slider'][$bs_slider_configuration];
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getValue('field_name')) {
      $form_state->setErrorByName('message', $this->t('The BS Slider plugin cannot be enabled if there is no field to be mapped.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['field_name'] = $form_state->getValue('field_name');

    $bs_slider_values = [];
    // Filter not enabled configurations.
    foreach ($form_state->getValue('bs_slider') as $key => $value) {
      if (!empty($value['plugin'])) {
        $bs_slider_values[$key] = $value['options'];
      }
    }
    $this->configuration['bs_slider'] = $bs_slider_values;
  }

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $form['bs_slider_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['paragraphs-plugin-inline-container'],
      ],
    ];

    $options = $this->bsSliderManager->getAllOptionSet();
    if (!empty($this->configuration['bs_slider'])) {
      $options = array_intersect_key($options, $this->configuration['bs_slider']);
    }
    $form['bs_slider_wrapper']['bs_slider'] = [
      '#type' => 'select',
      '#title' => $this->t('Slider'),
      '#options' => $options,
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), 'bs_slider'),
      '#empty_option' => $this->t('- None -'),
      '#attributes' => ['class' => ['paragraphs-plugin-form-element']],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $paragraph->setBehaviorSettings($this->pluginId, $form_state->getValues()['bs_slider_wrapper']);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(Paragraph $paragraph) {
    $bs_slider_optionset = $paragraph->getBehaviorSetting('bs_slider', NULL);
    $all_optionset = $this->bsSliderManager->getAllOptionSet();
    $summary = [];

    if (in_array($bs_slider_optionset, array_flip($all_optionset))) {
      $summary[] = $this->t('Slider settings: @settings', ['@settings' => $all_optionset[$bs_slider_optionset]]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {
    $behaviour_setting = $paragraph->getBehaviorSetting($this->getPluginId(), NULL);
    if (!$behaviour_setting) {
      return;
    }

    // Build paragraph items.
    $configuration = $this->getConfiguration();
    $field_name = $configuration['field_name'];
    if (isset($build[$field_name]) && isset($configuration['bs_slider'][$behaviour_setting['bs_slider']])) {
      /** @var \Drupal\bs_slider\Entity\BsSliderConfigurationInterface $bs_slider */
      $bs_slider = $this->bsSliderManager->entityLoad($behaviour_setting['bs_slider']);

      /** @var \Drupal\bs_slider\Plugin\BsSliderBase $plugin */
      if ($plugin = $this->bsSliderManager->getPlugin($behaviour_setting['bs_slider'])) {
        $plugin->view($build[$field_name], $bs_slider, $configuration['bs_slider'][$behaviour_setting['bs_slider']]);
      }
    }
  }

  protected function getEntityRefFieldViewModes(ParagraphsType $paragraphs_type, $field_name) {
    $field_definitions = $this->entityFieldManager->getFieldDefinitions('paragraph', $paragraphs_type->id());
    if (isset($field_definitions[$field_name])) {
      /** @var \Drupal\field\Entity\FieldConfig $field_config */
      $field_config = $field_definitions[$field_name];
      return $this->entityDisplayRepository->getViewModeOptions($field_config->getTargetBundle());
    }

    return NULL;
  }

  /**
   * Returns all fields that have cardinality greater than one.
   *
   * @param \Drupal\paragraphs\Entity\ParagraphsType $paragraphs_type
   *   The paragraph type to get the fields from.
   *
   * @return array
   *   A list of fields of the paragraph type,
   *   e.g. ['field_slides' => 'Slides', 'field_texts' => 'Texts'].
   */
  protected function getFieldsByCardinalityGreaterOne(ParagraphsType $paragraphs_type) {
    $fields = [];
    $field_definitions = $this->entityFieldManager->getFieldDefinitions('paragraph', $paragraphs_type->id());

    foreach ($field_definitions as $name => $definition) {
      if ($field_definitions[$name] instanceof FieldConfigInterface) {
        $cardinality = $definition->getFieldStorageDefinition()->getCardinality();

        if ($cardinality === 1) {
          continue;
        }

        $fields[$name] = $definition->getLabel();
      }
    }

    return $fields;
  }

}
