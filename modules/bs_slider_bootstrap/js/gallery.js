/**
 * @file
 * BS Slider Bootstrap Gallery JS functionality.
 */
(function ($, Drupal, once) {

  'use strict';

  const galleryClass = 'bs-slider--bootstrap-gallery-grid';

  /**
   * bsSliderBootstrapGallery behaviour.
   */
  Drupal.behaviors.bsSliderBootstrapGallery = {
    attach: function (context, settings) {
      // If context is gallery itself we need to switch to context parent.
      let _context = context;
      if (_context.classList && _context.classList.contains(galleryClass)) {
        _context = context.parentNode;
      }

      once('bs-slider-bootstrap-gallery', '.' + galleryClass, _context).forEach(el => {
        const $slider = $(el);
        const $thumbnails = $slider.find('.bs-slider__thumbnails .bs-slider__item');
        const $gallery = $slider.find('.bs-slider__gallery');
        const $galleryCarousel = $gallery.find('.bs-slider--bootstrap-carousel');
        const $closeButton = $gallery.find('.bs-slider__close');
        let isVisible = false;
        let currentIndex = 0;

        function showBg() {
          $gallery.addClass('bs-slider--visible');
          bodyScrollLock.disableBodyScroll($gallery[0], {
            reserveScrollBarGap: true
          });
        }

        function hideBg() {
          $gallery.removeClass('bs-slider--visible');
          bodyScrollLock.enableBodyScroll($gallery[0]);
        }

        function show() {
          $gallery.addClass('bs-slider--show');
          isVisible = true;
        }

        function hide() {
          $gallery.removeClass('bs-slider--show');
          $galleryCarousel.carousel('dispose');
          isVisible = false;
        }

        $thumbnails.each(function (index) {
          this.addEventListener('click', e => {
            $galleryCarousel.carousel(index);

            // Show carousel if it's not shown.
            if (!isVisible) {
              if (index === currentIndex) {
                showBg();
                show();
              }
              else {
                showBg();
                // Wait for carousel to finish slide transition before show.
                // @todo - can we optimize this somehow so we jump to the
                // slide immediately in Bootstrap Carousel?
                // Possibly we need to override transition CSS values that
                // are used here.
                // @see http://getbootstrap.com/docs/4.1/components/carousel/#change-transition-duration
                $galleryCarousel.one('slid.bs.carousel', function () {
                  show();
                });
              }
              currentIndex = index;
            }
          });
        });

        $closeButton.click(function () {
          hide();
          hideBg();
        });
      });
    }
  };

})(jQuery, Drupal, once);
