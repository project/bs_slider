/**
 * @file
 * BS Slider Tiny Slider JS functionality.
 */
(function (Drupal, tns) {

  'use strict';

  /**
   * bsSliderTinySlider behaviour.
   */
  Drupal.behaviors.bsSliderTinySlider = {
    attach: function (context, settings) {
      // If context is gallery itself we need to switch to context parent.
      let _context = context;
      if (_context.dataset && _context.dataset.bsSlider === 'tiny-slider') {
        _context = context.parentNode;
      }

      const sliders = _context.querySelectorAll('[data-bs-slider="tiny-slider"]');
      sliders.forEach(function (slider) {
        if (!slider.dataset.bsSliderInit) {
          // Mark slider as initialized.
          slider.dataset.bsSliderInit = true;

          const options = JSON.parse(slider.dataset.bsSliderOptions);

          // Specify that this element is a slider container element.
          options.container = slider;

          // Initialize slider and save it to slider node.
          slider.bsSlider = tns(options);
        }
      });
    }
  };

})(Drupal, tns);
