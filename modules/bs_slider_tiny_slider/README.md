CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Features
 * Configuration
 * Todo
 * Maintainers


INTRODUCTION
------------

This modules integrate [Tiny Slider](https://github.com/ganlanyuan/tiny-slider)
javascript library.


REQUIREMENTS
------------

You need to download Tiny Slider library from. This module has been tested with
version 2.9.3. After download extract files to libraries/tiny-slider.

In case you are using composer for your project (recommended) then execute next
commands:

```shell script
composer config repositories.tiny_slider '{"type": "package", "package": {"name": "ganlanyuan/tiny-slider", "version": "2.9.3", "type": "drupal-library", "dist": {"url": "https://github.com/ganlanyuan/tiny-slider/archive/v2.9.3.zip", "type": "zip"}, "require": {"composer/installers": "^1.2.0"}}}';
composer require ganlanyuan/tiny-slider;
```


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


FEATURES
--------

@TODO


CONFIGURATION
-------------

@TODO


MAINTAINERS
-----------

Current maintainers:
 * Ivica Puljic (pivica) - https://www.drupal.org/u/pivica
